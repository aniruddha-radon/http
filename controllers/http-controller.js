app.controller("httpController", function ($scope, dataService) {
    //the $scope is the model. it acts as the glue between the view (HTML) and the controller.
    //Anything specified on the model is accessible in the view.
    //Anything NOT specified on the model is NOT accessible in the view.

    //dataService is the service we created in the services folder.
    //we do NOT give the file name as the dependency but the services name -> dataService.

    $scope.posts = [];

    $scope.init = function() {
        dataService.getPosts().then(function(result) {
            $scope.posts = result.data;
        })
    }

    $scope.getComments = function(id) {
        dataService.getComments(id).then(function(result) {
            var index = $scope.posts.findIndex(post => post.id === id);
            $scope.posts[index]['comments'] = result.data;
        })
    }

    $scope.init();
})